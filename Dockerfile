FROM golang AS builder

RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main
RUN curl -O https://curl.haxx.se/ca/cacert.pem

FROM scratch
COPY --from=builder /app/cacert.pem /etc/ssl/certs/
COPY --from=builder /app/main /
CMD ["/main"]
