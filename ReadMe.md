# Drone CI activity feed as RSS

A simple Go application to provide the activity feed within drone CI as an RSS feed.

There is no configuration, the URL to Drone CI and the access token are provided in the HTTP request for the RSS feed.
