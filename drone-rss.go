package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/feeds"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

type BuildInfo struct {
	Number   int    `json:"number"`
	Status   string `json:"status"`
	Message  string `json:"message"`
	Source   string `json:"source"`
	Finished int    `json:"finished"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	droneServers, ok := r.URL.Query()["drone-server"]
	droneServer := ""
	if ok {
		droneServer = droneServers[0]
	} else {
		droneServer = "https://cloud.drone.io"
	}

	droneToken, hasDroneToken := r.URL.Query()["drone-token"]

	owner, ok := r.URL.Query()["owner"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Missing owner parameter")
		return
	}

	repo, ok := r.URL.Query()["repo"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Missing repo parameter")
		return
	}

	request, err := http.NewRequest("GET", droneServer+"/api/repos/"+owner[0]+"/"+repo[0]+"/builds", nil)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
		return
	}

	if hasDroneToken {
		request.Header.Set("Authorization", "Bearer "+droneToken[0])
	}

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
		return
	}

	var builds []BuildInfo
	err = json.NewDecoder(response.Body).Decode(&builds)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
		return
	}

	feed := &feeds.Feed{
		Title: fmt.Sprintf("Activity Feed for %s/%s", owner[0], repo[0]),
		Link:  &feeds.Link{Href: fmt.Sprintf("%s/%s/%s", droneServer, owner[0], repo[0])},
	}

	feed.Items = make([]*feeds.Item, len(builds))

	for i, build := range builds {
		feed.Items[i] = &feeds.Item{
			Title:       fmt.Sprintf("Build %d on %s: %s", build.Number, build.Source, build.Status),
			Link:        &feeds.Link{Href: fmt.Sprintf("%s/%s/%s/%d", droneServer, owner[0], repo[0], build.Number)},
			Description: build.Message,
			Created:     time.Unix(int64(build.Finished), 0),
		}
	}

	w.Header().Add("Content-Type", "application/rss+xml")
	rss, err := feed.ToRss()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
		return
	}

	io.WriteString(w, rss)
}

func determineListenAddress() string {
	if value, ok := os.LookupEnv("LISTEN_ADDRESS"); ok {
		return value
	}
	return ":8080"
}

func main() {
	http.HandleFunc("/activity-feed", handler)
	log.Fatal(http.ListenAndServe(determineListenAddress(), nil))
}
